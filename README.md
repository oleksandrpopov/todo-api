# Todo API

A simple implementation of REST Todo API with Containers, Event-Driven Architecture, CQRS.
The main purpose of the app is to demonstrate communication between app components and container.
So, some things are omitted (such as validation, logging, error handling, health checking). 

## Services

See more details in the `docker-compose.yml`

### app

The main service that implements REST Todo API using Event-Driven, CQRS approaches.
The service publishes messages to Redis instance in order to notify other service about domain events.

The service is implemented using CodeIgniter 4 framework.
It was chosen because I haven't used the version 4 before and it's a good opportunity to check how it evolved over the past years. 

Third party packages:

- `php-di/php-di` Unfortunately, CodeIgniter doesn't provide a DI container, so I had to integrate a third party solution. 
- `league/tactician` A simple command bus.
- `symfony/event-dispatcher` PSR-14: Event Dispatcher.
- `league/fractal` Used to transform API resources.
- `enqueue/simple-client` Messaging solution for php.

### audit

A simple CLI app that connects to the Redis instance and prints received events to the console.

### database

Just MySQL instance.

### redis

Redis instance used as an event bus between services.

## Requirements

 - Docker
 
## Build & Run

To build and run the application it's suggested to use `docker-compose up` command, otherwise you won't be able to see output from the audit service. 
It's important to run migrations at the first launch. To do that just connect to the app service using `docker-compose exec app bash` and execute 
the `php spark migrate` command.

## Tests

To run test simply connect to the app service using `docker-compose exec app bash` and run `composer run-script test`.

# API Requests

GET /todos

```
curl --request GET \
  --url http://localhost:8080/todos
```

GET /todos/:id

```
curl --request GET \
  --url http://localhost:8080/todos/14d6b407-c297-11ea-a58a-0242ac130002
```

POST /todos

```
curl --request POST \
  --url http://localhost:8080/todos \
  --header 'content-type: application/json' \
  --data '{
	"id": "d4a5b102-c296-11ea-a58a-0242ac130003",
	"title": "second",
	"completed": false
}
'
```

GET /todos/:id

```
curl --request PUT \
  --url http://localhost:8080/todos/d4a5b102-c296-11ea-a58a-0242ac130003 \
  --header 'content-type: application/json' \
  --data '{
	"title": "first",
	"completed": true
}
'
```

DELETE /todos/:id

```
curl --request DELETE \
  --url http://localhost:8080/todos/d4a5b102-c296-11ea-a58a-0242ac130003 \
  --header 'content-type: application/json'
```
