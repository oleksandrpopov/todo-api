<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

// A simple audit script that listens for published events.

use Enqueue\SimpleClient\SimpleClient;
use Interop\Queue\Message;
use Interop\Queue\Processor;

require "vendor/autoload.php";

$dsn = sprintf('redis://%s:%s', getenv('REDIS_HOST'), getenv('REDIS_PORT'));
$client = new SimpleClient($dsn);

$client->bindTopic('todo-updated', function(Message $psrMessage) {
    echo "Todo has been updated: ", $psrMessage->getBody(), PHP_EOL;
    return Processor::ACK;
});

$client->bindTopic('todo-created', function(Message $psrMessage) {
    echo "Todo has been created: ", $psrMessage->getBody(), PHP_EOL;
    return Processor::ACK;
});

$client->bindTopic('todo-deleted', function(Message $psrMessage) {
    echo "Todo has been deleted: ", $psrMessage->getBody(), PHP_EOL;
    return Processor::ACK;
});

$client->consume();
