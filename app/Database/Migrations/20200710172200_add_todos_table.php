<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

class AddTodosTable extends \CodeIgniter\Database\Migration
{
    public function up()
    {
        $this->db->query(
            "CREATE TABLE `todos` (
              `id` CHAR(36) NOT NULL,
              `title` VARCHAR(255) NOT NULL,
              `completed` TINYINT(1) UNSIGNED NOT NULL,
              PRIMARY KEY (`id`));"
        );
    }

    public function down()
    {
        $this->db->query("DROP TABLE `todos`;");
    }
}
