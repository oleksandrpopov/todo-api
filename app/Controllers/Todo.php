<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Controllers;

use App\Resources\Transformers\TodoTransformer;
use App\Todo\Commands\CreateTodoCommand;
use App\Todo\Commands\DeleteTodoCommand;
use App\Todo\Commands\UpdateTodoCommand;
use App\Todo\Exceptions\TodoNotFoundException;
use App\Todo\Queries\GetTodoQuery;
use App\Todo\Queries\GetTodosQuery;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class Todo extends BaseController
{
    /**
     * @Inject
     * @var \App\Cqrs\QueryBusInterface
     */
    private $queryBus;

    /**
     * @Inject
     * @var \App\Cqrs\CommandBusInterface
     */
    private $commandBus;

    /**
     * @Inject
     * @var \League\Fractal\Manager
     */
    private $fractal;

    public function index()
    {
        $todos = $this->queryBus->execute(new GetTodosQuery());
        $data = $this->fractal->createData(
            new Collection($todos, new TodoTransformer())
        );

        return $this->respond($data->toArray());
    }

    public function create()
    {
        $data = $this->request->getJSON(true);
        $this->commandBus->execute(new CreateTodoCommand($data['id'], $data['title'], $data['completed']));

        return $this->respondCreated();
    }

    public function update($id = null)
    {
        $data = $this->request->getJSON(true);

        try {
            $this->commandBus->execute(new UpdateTodoCommand($id, $data['title'], $data['completed']));
        } catch (TodoNotFoundException $exception) {
            return $this->failNotFound($exception->getMessage());
        }

        return $this->respondUpdated();
    }

    public function show($id = null)
    {
        try {
            $todo = $this->queryBus->execute(new GetTodoQuery($id));
        } catch (TodoNotFoundException $exception) {
            return $this->failNotFound($exception->getMessage());
        }

        $data = $this->fractal->createData(
            new Item($todo, new TodoTransformer())
        );

        return $this->respond($data->toArray());
    }

    public function delete($id = null)
    {
        try {
            $this->commandBus->execute(new DeleteTodoCommand($id));
        } catch (TodoNotFoundException $exception) {
            return $this->failNotFound($exception->getMessage());
        }

        return $this->respondDeleted();
    }
}
