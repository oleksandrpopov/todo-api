<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Resources\Transformers;

use App\Todo\Entities\TodoInterface;
use League\Fractal\TransformerAbstract;

class TodoTransformer extends TransformerAbstract
{
    /**
     * @param TodoInterface $todo
     *
     * @return array
     */
    public function transform(TodoInterface $todo)
    {
        return [
            'id' => $todo->getId(),
            'title' => $todo->getTitle(),
            'completed' => $todo->isCompleted(),
        ];
    }
}
