<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\PubSub\Todo;

use App\PubSub\EventPublisherInterface;
use App\Todo\Events\TodoCreatedEvent;
use App\Todo\Events\TodoDeletedEvent;
use App\Todo\Events\TodoEvent;
use App\Todo\Events\TodoUpdatedEvent;

class EventPublisher
{
    /**
     * @var EventPublisherInterface
     */
    private $eventPublisher;

    public function __construct(EventPublisherInterface $eventPublisher)
    {
        $this->eventPublisher = $eventPublisher;
    }

    public function onTodoCreated(TodoCreatedEvent $event): void
    {
        $this->eventPublisher->publishEvent(
            'todo-created',
            $this->serializeEvent($event)
        );
    }

    public function onTodoUpdated(TodoUpdatedEvent $event): void
    {
        $this->eventPublisher->publishEvent(
            'todo-updated',
            $this->serializeEvent($event)
        );
    }

    public function onTodoDeleted(TodoDeletedEvent $event): void
    {
        $this->eventPublisher->publishEvent(
            'todo-deleted',
            $this->serializeEvent($event)
        );
    }

    private function serializeEvent(TodoEvent $event): string
    {
        return json_encode(['id' => $event->getTodo()->getId()]);
    }
}
