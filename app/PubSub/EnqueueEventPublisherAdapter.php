<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\PubSub;

use Enqueue\SimpleClient\SimpleClient;

class EnqueueEventPublisherAdapter implements EventPublisherInterface
{
    /**
     * @var SimpleClient
     */
    private $simpleClient;

    public function __construct(SimpleClient $simpleClient)
    {
        $this->simpleClient = $simpleClient;
    }

    public function publishEvent(string $eventName, string $message): void
    {
        $this->simpleClient->sendEvent($eventName, $message);
    }
}
