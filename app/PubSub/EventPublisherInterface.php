<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\PubSub;

interface EventPublisherInterface
{
    /**
     * @param string $eventName
     * @param string $message
     */
    public function publishEvent(string $eventName, string $message): void;
}
