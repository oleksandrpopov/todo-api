<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

return [
    \App\Todo\Events\TodoCreatedEvent::class => [\App\PubSub\Todo\EventPublisher::class, 'onTodoCreated'],
    \App\Todo\Events\TodoUpdatedEvent::class => [\App\PubSub\Todo\EventPublisher::class, 'onTodoUpdated'],
    \App\Todo\Events\TodoDeletedEvent::class => [\App\PubSub\Todo\EventPublisher::class, 'onTodoDeleted'],
];
