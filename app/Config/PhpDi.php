<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

use App\PubSub\EnqueueEventPublisherAdapter;
use App\PubSub\EventPublisherInterface;
use App\Todo\Repositories\TodoRepository;
use Enqueue\SimpleClient\SimpleClient;
use League\Fractal\Manager;
use Psr\Container\ContainerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use League\Tactician\CommandBus;
use League\Tactician\Container\ContainerLocator;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;
use Symfony\Component\EventDispatcher\EventDispatcher;
use function DI\create;
use function DI\get;
use App\Cqrs\CommandBusInterface;
use App\Cqrs\QueryBusInterface;
use App\Cqrs\TacticianBusAdapter;
use App\Todo\Repositories\TodoRepositoryInterface;

return [
    // Configure command and query busses.
    // For the demo purpose we are using the same bus for commands and queries.
    CommandBusInterface::class => get(TacticianBusAdapter::class),

    QueryBusInterface::class => get(TacticianBusAdapter::class),

    // A factor that creates TacticianBusAdapter.
    // It's here to simplify things in the demo in a real world app there probably will be an abstraction and a factory class that implements it.
    TacticianBusAdapter::class => function (ContainerInterface $container) {
        $queryToHandlerMap = require APPPATH . 'Config/QueryHandlers.php';
        $commandToHandlerMap = require APPPATH . 'Config/CommandHandlers.php';
        $containerLocator = new ContainerLocator($container, array_merge($queryToHandlerMap, $commandToHandlerMap));
        $commandHandlerMiddleware = new CommandHandlerMiddleware(new ClassNameExtractor(), $containerLocator, new HandleInflector());

        return new TacticianBusAdapter(new CommandBus([$commandHandlerMiddleware]));
    },

    // PSR-14: Event Dispatcher (thanks to Symfony for implementing it)
    // To simplify things we are adding event listeners here. Alternatively we can do that in a system hook.
    EventDispatcherInterface::class => function (EventDispatcher $eventDispatcher, ContainerInterface $container) {
        $eventToHandlerMap = require APPPATH . 'Config/EventHandlers.php';
        foreach ($eventToHandlerMap as $event => [$handlerClass, $handlerMethod]) {
            $eventDispatcher->addListener($event, [$container->get($handlerClass), $handlerMethod]);
        }

        return $eventDispatcher;
    },

    TodoRepositoryInterface::class => function () {
        return new TodoRepository(Config\Database::connect()->table('todos'));
    },

    EventPublisherInterface::class => function () {
        $dsn = sprintf('redis://%s:%s', getenv('REDIS_HOST'), getenv('REDIS_PORT'));

        return new EnqueueEventPublisherAdapter(new SimpleClient($dsn));
    },

    Manager::class => create(Manager::class),
];
