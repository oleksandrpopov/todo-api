<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

/**
 * query -> handler map
 */
return [
    \App\Todo\Queries\GetTodoQuery::class => \App\Todo\Queries\GetTodoHandler::class,
    \App\Todo\Queries\GetTodosQuery::class => \App\Todo\Queries\GetTodosHandler::class,
];
