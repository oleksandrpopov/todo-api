<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

/**
 * command -> handler map
 */
return [
    \App\Todo\Commands\CreateTodoCommand::class => \App\Todo\Commands\CreateTodoHandler::class,
    \App\Todo\Commands\UpdateTodoCommand::class => \App\Todo\Commands\UpdateTodoHandler::class,
    \App\Todo\Commands\DeleteTodoCommand::class => \App\Todo\Commands\DeleteTodoHandler::class,
];
