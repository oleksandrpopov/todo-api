<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Cqrs;

use League\Tactician\CommandBus;

/**
 * Class TacticianBusAdapter
 * A simple adapter for League\Tactician\CommandBus that implements our own CommandBusInterface and QueryBusInterface.
 *
 * @package App\Cqrs
 */
class TacticianBusAdapter implements CommandBusInterface, QueryBusInterface
{
    /**
     * @var CommandBus
     */
    private $bus;

    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param mixed $command
     *
     * @throws \Exception
     */
    public function execute($command)
    {
        return $this->bus->handle($command);
    }
}
