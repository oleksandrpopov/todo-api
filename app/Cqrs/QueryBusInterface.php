<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Cqrs;

/**
 * Interface QueryBusInterface
 * Interface for the app command bus.
 * App components should rely on this interface and not on a vendor package.
 *
 * @package App\Cqrs
 */
interface QueryBusInterface
{
    /**
     * @param $query
     *
     * @return mixed
     */
    public function execute($query);
}
