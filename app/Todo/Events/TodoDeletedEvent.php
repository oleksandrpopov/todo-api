<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Events;

class TodoDeletedEvent extends TodoEvent
{
}
