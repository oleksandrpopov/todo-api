<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Events;

use App\Todo\Entities\TodoInterface;

class TodoEvent
{
    /**
     * @var TodoInterface
     */
    private $todo;

    public function __construct(TodoInterface $todo)
    {
        $this->todo = $todo;
    }

    /**
     * @return TodoInterface
     */
    public function getTodo(): TodoInterface
    {
        return $this->todo;
    }
}
