<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Repositories;

use App\Todo\Entities\TodoInterface;

interface TodoRepositoryInterface
{
    /**
     * Saves a todo.
     *
     * @param TodoInterface $todo
     */
    public function save(TodoInterface $todo): void;

    /**
     * Deletes a todo by id.
     *
     * @param string $todoId
     */
    public function delete(string $todoId): void;

    /**
     * Returns a todo or null when Todo is not found.
     *
     * @param string $todoId
     *
     * @return TodoInterface|null
     */
    public function findById(string $todoId): ?TodoInterface;

    /**
     * Returns a todo or fails.
     *
     * @param string $todoId
     *
     * @return TodoInterface
     * @throws \App\Todo\Exceptions\TodoNotFoundException
     */
    public function getById(string $todoId): TodoInterface;

    /**
     * Returns a list of todos.
     *
     * @return array|TodoInterface[]
     */
    public function getAll(): array;
}
