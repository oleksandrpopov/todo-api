<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Repositories;

use App\Todo\Entities\Todo;
use App\Todo\Entities\TodoInterface;
use App\Todo\Exceptions\TodoNotFoundException;
use CodeIgniter\Database\BaseBuilder;

class TodoRepository implements TodoRepositoryInterface
{
    /**
     * @var BaseBuilder
     */
    private $queryBuilder;

    public function __construct(BaseBuilder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @inheritDoc
     */
    public function save(TodoInterface $todo): void
    {
        if ($this->todoExists($todo->getId())) {
            $this->queryBuilder->update([
                'title' => $todo->getTitle(),
                'completed' => $todo->isCompleted(),
            ], ['id' => $todo->getId()]);
        } else {
            $this->queryBuilder->insert([
                'id' => $todo->getId(),
                'title' => $todo->getTitle(),
                'completed' => $todo->isCompleted(),
            ]);
        }
    }

    /**
     * @inheritDoc
     */
    public function delete(string $todoId): void
    {
        $this->queryBuilder->delete(['id' => $todoId]);
    }

    /**
     * @inheritDoc
     */
    public function findById(string $todoId): ?TodoInterface
    {
        $row = $this->queryBuilder->getWhere(['id' => $todoId])->getRow();

        return $row ? $this->rowToTodo($row) : null;
    }

    /**
     * @inheritDoc
     */
    public function getById(string $todoId): TodoInterface
    {
        $todo = $this->findById($todoId);

        if ($todo) {
            return $todo;
        }

        throw new TodoNotFoundException("Failed to find a todo with id: {$todoId}.");
    }

    /**
     * @inheritDoc
     */
    public function getAll(): array
    {
        return array_map(
            [$this, 'rowToTodo'],
            $this->queryBuilder->get()->getResult()
        );
    }

    /**
     * @param string $todoId
     *
     * @return bool
     */
    protected function todoExists(string $todoId): bool
    {
        return $this->queryBuilder->where('id', $todoId)->countAllResults() > 0;
    }

    /**
     * @param \stdClass $row
     *
     * @return Todo
     */
    protected function rowToTodo(\stdClass $row)
    {
        $todo = new Todo();
        $todo->setId($row->id);
        $todo->setTitle($row->title);
        $todo->setCompleted(boolval($row->completed));

        return $todo;
    }
}
