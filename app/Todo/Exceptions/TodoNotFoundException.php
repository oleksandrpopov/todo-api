<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Exceptions;

class TodoNotFoundException extends \Exception
{
}
