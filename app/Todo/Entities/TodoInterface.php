<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Entities;

interface TodoInterface
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @param string $id
     */
    public function setId(string $id): void;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @param string $title
     */
    public function setTitle(string $title): void;

    /**
     * @return bool
     */
    public function isCompleted(): bool;

    /**
     * @param bool $completed
     */
    public function setCompleted(bool $completed): void;
}
