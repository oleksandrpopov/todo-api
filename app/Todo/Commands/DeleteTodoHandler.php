<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Commands;

use App\Todo\Events\TodoDeletedEvent;

class DeleteTodoHandler extends BasicHandler
{
    /**
     * @param DeleteTodoCommand $command
     *
     * @throws \App\Todo\Exceptions\TodoNotFoundException
     */
    public function handle(DeleteTodoCommand $command)
    {
        $todo = $this->todoRepository->getById($command->getTodoId());
        $this->todoRepository->delete($command->getTodoId());
        $this->eventDispatcher->dispatch(new TodoDeletedEvent($todo));
    }
}
