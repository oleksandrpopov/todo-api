<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Commands;

use App\Todo\Entities\Todo;
use App\Todo\Events\TodoCreatedEvent;

class CreateTodoHandler extends BasicHandler
{
    /**
     * @param CreateTodoCommand $command
     */
    public function handle(CreateTodoCommand $command)
    {
        $todo = new Todo();
        $todo->setId($command->getId());
        $todo->setTitle($command->getTitle());
        $todo->setCompleted($command->isCompleted());
        $this->todoRepository->save($todo);
        $this->eventDispatcher->dispatch(new TodoCreatedEvent($todo));
    }
}
