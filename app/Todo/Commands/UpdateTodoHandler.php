<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Commands;

use App\Todo\Events\TodoUpdatedEvent;

class UpdateTodoHandler extends BasicHandler
{
    /**
     * @param UpdateTodoCommand $command
     *
     * @throws \App\Todo\Exceptions\TodoNotFoundException
     */
    public function handle(UpdateTodoCommand $command)
    {
        $todo = $this->todoRepository->getById($command->getId());
        $todo->setTitle($command->getTitle());
        $todo->setCompleted($command->isCompleted());
        $this->todoRepository->save($todo);
        $this->eventDispatcher->dispatch(new TodoUpdatedEvent($todo));
    }
}
