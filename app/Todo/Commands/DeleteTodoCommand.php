<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Commands;

class DeleteTodoCommand
{
    /**
     * @var string
     */
    private $todoId;

    public function __construct(string $todoId)
    {
        $this->todoId = $todoId;
    }

    /**
     * @return string
     */
    public function getTodoId(): string
    {
        return $this->todoId;
    }
}
