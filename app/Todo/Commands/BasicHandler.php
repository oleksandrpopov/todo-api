<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Commands;

use App\Todo\Repositories\TodoRepositoryInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

class BasicHandler
{
    /**
     * @var TodoRepositoryInterface
     */
    protected $todoRepository;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    public function __construct(TodoRepositoryInterface $todoRepository, EventDispatcherInterface $eventDispatcher)
    {
        $this->todoRepository = $todoRepository;
        $this->eventDispatcher = $eventDispatcher;
    }
}
