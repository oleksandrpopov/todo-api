<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Queries;

use App\Todo\Repositories\TodoRepositoryInterface;

class BasicHandler
{
    /**
     * @var TodoRepositoryInterface
     */
    protected $todoRepository;

    public function __construct(TodoRepositoryInterface $todoRepository)
    {
        $this->todoRepository = $todoRepository;
    }
}
