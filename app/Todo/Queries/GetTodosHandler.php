<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Queries;

class GetTodosHandler extends BasicHandler
{
    /**
     * @param GetTodosQuery $query
     *
     * @return \App\Todo\Entities\TodoInterface[]|array
     */
    public function handle(GetTodosQuery $query)
    {
        return $this->todoRepository->getAll();
    }
}
