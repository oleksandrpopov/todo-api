<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace App\Todo\Queries;

class GetTodoHandler extends BasicHandler
{
    /**
     * @param GetTodoQuery $query
     *
     * @return \App\Todo\Entities\TodoInterface
     * @throws \App\Todo\Exceptions\TodoNotFoundException
     */
    public function handle(GetTodoQuery $query)
    {
        return $this->todoRepository->getById($query->getTodoId());
    }
}
