FROM php:7.4-apache

# Copy source code
WORKDIR /var/www/html/
COPY . /var/www/html/

RUN echo "\n## Install system packages\n"; \
    apt-get update && \
    apt-get install -y \
       unzip \
       libicu-dev \
       libzip-dev;

RUN pecl install -o -f redis;

RUN echo "\n## Enable php extensions\n"; \
    docker-php-source extract; \
    docker-php-ext-configure zip --with-libzip; \
    docker-php-ext-install zip; \
    docker-php-ext-install mysqli; \
    docker-php-ext-install pdo_mysql; \
    docker-php-ext-configure intl; \
    docker-php-ext-install intl; \
    docker-php-ext-enable redis; \
    docker-php-source delete;

RUN echo "\n## Install composer\n"; \
    php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer; \
    echo "\n## Install app dependencies\n"; \
    composer install;

RUN echo "\n## Enable apache modules\n"; \
    a2enmod rewrite; \
    a2enmod headers; \
    a2enmod deflate;

RUN echo "\n## Use the default production configuration\n"; \
    mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini";

RUN echo "\n## Change DocumentRoot\n"; \
    sed -ri -e 's!/var/www/html!/var/www/html/public!g' /etc/apache2/sites-available/*.conf; \
    sed -ri -e 's!/var/www/!/var/www/html/public!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf;

RUN echo "\n## Configure Apache HTTP Server to listen on a port specified with PORT env variable\n"; \
    sed -i '/^\s*Listen 80/c\Listen ${PORT}' /etc/apache2/ports.conf \
        && sed -i '/^\s*<VirtualHost \*:80>/c\<VirtualHost \*:${PORT}>' /etc/apache2/sites-available/000-default.conf;
