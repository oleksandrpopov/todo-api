<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\Resources\Transformers;

use App\Resources\Transformers\TodoTransformer;
use App\Todo\Entities\TodoInterface;
use CodeIgniter\Test\CIUnitTestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class TodoTransformerTest extends CIUnitTestCase
{
    use MockeryPHPUnitIntegration;

    /** @test */
    public function transformsTodoToArray()
    {
        $id = 'my-uuid';
        $title = 'to something';
        $completed = true;
        $expected = [
            'id' => $id,
            'title' => $title,
            'completed' => $completed,
        ];

        $todo = \Mockery::mock(TodoInterface::class);
        $todo->shouldReceive('getId')
            ->once()
            ->andReturn($id);
        $todo->shouldReceive('getTitle')
            ->once()
            ->andReturn($title);
        $todo->shouldReceive('isCompleted')
            ->once()
            ->andReturn($completed);

        $transformer = new TodoTransformer();

        $this->assertEquals($expected, $transformer->transform($todo));
    }
}
