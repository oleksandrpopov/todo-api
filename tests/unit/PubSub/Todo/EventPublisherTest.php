<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\PubSub\Todo;

use App\PubSub\EventPublisherInterface;
use App\PubSub\Todo\EventPublisher;
use App\Todo\Entities\TodoInterface;
use App\Todo\Events\TodoCreatedEvent;
use App\Todo\Events\TodoDeletedEvent;
use App\Todo\Events\TodoUpdatedEvent;
use CodeIgniter\Test\CIUnitTestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class EventPublisherTest extends CIUnitTestCase
{
    use MockeryPHPUnitIntegration;

    /**
     * @var EventPublisherInterface|\Mockery\MockInterface
     */
    private $eventPublisher;

    /**
     * @var EventPublisher
     */
    private $publisher;

    /** @test */
    public function publishesTodoCreatedEvent()
    {
        $id = 'my-uuid';
        $todo = \Mockery::mock(TodoInterface::class);
        $todo->shouldReceive('getId')
            ->once()
            ->andReturn($id);
        $event = \Mockery::mock(TodoCreatedEvent::class);
        $event->shouldReceive('getTodo')
            ->once()
            ->andReturn($todo);
        $this->eventPublisher->shouldReceive('publishEvent')
            ->once()
            ->with('todo-created', '{"id":"my-uuid"}');

        $this->publisher->onTodoCreated($event);
    }

    /** @test */
    public function publishesTodoUpdatedEvent()
    {
        $id = 'my-uuid';
        $todo = \Mockery::mock(TodoInterface::class);
        $todo->shouldReceive('getId')
            ->once()
            ->andReturn($id);
        $event = \Mockery::mock(TodoUpdatedEvent::class);
        $event->shouldReceive('getTodo')
            ->once()
            ->andReturn($todo);
        $this->eventPublisher->shouldReceive('publishEvent')
            ->once()
            ->with('todo-updated', '{"id":"my-uuid"}');

        $this->publisher->onTodoUpdated($event);
    }

    /** @test */
    public function publishesTodoDeletedEvent()
    {
        $id = 'my-uuid';
        $todo = \Mockery::mock(TodoInterface::class);
        $todo->shouldReceive('getId')
            ->once()
            ->andReturn($id);
        $event = \Mockery::mock(TodoDeletedEvent::class);
        $event->shouldReceive('getTodo')
            ->once()
            ->andReturn($todo);
        $this->eventPublisher->shouldReceive('publishEvent')
            ->once()
            ->with('todo-deleted', '{"id":"my-uuid"}');

        $this->publisher->onTodoDeleted($event);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->eventPublisher = \Mockery::mock(EventPublisherInterface::class);
        $this->publisher = new EventPublisher($this->eventPublisher);
    }
}
