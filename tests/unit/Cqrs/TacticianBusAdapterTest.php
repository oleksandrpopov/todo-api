<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\Cqrs;

use App\Cqrs\TacticianBusAdapter;
use CodeIgniter\Test\CIUnitTestCase;
use League\Tactician\CommandBus;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class TacticianBusAdapterTest extends CIUnitTestCase
{
    use MockeryPHPUnitIntegration;

    /** @test */
    public function delegatesHandlingToTactician()
    {
        $bus = \Mockery::mock(CommandBus::class);
        $bus->shouldReceive('handle')
            ->once()
            ->with('command')
            ->andReturn('result');

        $adapter = new TacticianBusAdapter($bus);

        $this->assertEquals('result', $adapter->execute('command'));
    }
}
