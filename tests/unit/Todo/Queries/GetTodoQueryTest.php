<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\Todo\Queries;

use App\Todo\Queries\GetTodoQuery;
use CodeIgniter\Test\CIUnitTestCase;

class GetTodoQueryTest extends CIUnitTestCase
{
    /** @test */
    public function implementsGetter()
    {
        $query = new GetTodoQuery('my-uuid');

        $this->assertEquals('my-uuid', $query->getTodoId());
    }
}
