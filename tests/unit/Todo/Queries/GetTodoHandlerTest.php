<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\Todo\Queries;

use App\Todo\Entities\TodoInterface;
use App\Todo\Queries\GetTodoHandler;
use App\Todo\Queries\GetTodoQuery;
use App\Todo\Repositories\TodoRepositoryInterface;
use CodeIgniter\Test\CIUnitTestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class GetTodoHandlerTest extends CIUnitTestCase
{
    use MockeryPHPUnitIntegration;

    /** @test */
    public function handlesQuery()
    {
        $id = 'my-uuid';
        $query = \Mockery::mock(GetTodoQuery::class);
        $query->shouldReceive('getTodoId')
            ->once()
            ->andReturn($id);

        $todo = \Mockery::mock(TodoInterface::class);
        $todoRepo = \Mockery::mock(TodoRepositoryInterface::class);
        $todoRepo->shouldReceive('getById')
            ->once()
            ->with($id)
            ->andReturn($todo);

        $handler = new GetTodoHandler($todoRepo);

        $this->assertEquals($todo, $handler->handle($query));
    }
}
