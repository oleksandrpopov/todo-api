<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\Todo\Queries;

use App\Todo\Entities\TodoInterface;
use App\Todo\Queries\GetTodosQuery;
use App\Todo\Queries\GetTodosHandler;
use App\Todo\Repositories\TodoRepositoryInterface;
use CodeIgniter\Test\CIUnitTestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class GetTodosHandlerTest extends CIUnitTestCase
{
    use MockeryPHPUnitIntegration;

    /** @test */
    public function handlesQuery()
    {
        $query = \Mockery::mock(GetTodosQuery::class);
        $todo = \Mockery::mock(TodoInterface::class);
        $todoRepo = \Mockery::mock(TodoRepositoryInterface::class);
        $todoRepo->shouldReceive('getAll')
            ->once()
            ->andReturn([$todo]);

        $handler = new GetTodosHandler($todoRepo);

        $this->assertEquals([$todo], $handler->handle($query));
    }
}
