<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\Todo\Commands;

use App\Todo\Commands\DeleteTodoCommand;
use App\Todo\Commands\DeleteTodoHandler;
use App\Todo\Entities\Todo;
use App\Todo\Events\TodoDeletedEvent;
use App\Todo\Repositories\TodoRepositoryInterface;
use CodeIgniter\Test\CIUnitTestCase;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\EventDispatcherInterface;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class DeleteTodoHandlerTest extends CIUnitTestCase
{
    use MockeryPHPUnitIntegration;

    /** @test */
    public function handlesCommand()
    {
        $id = 'my-uuid';

        $command = \Mockery::mock(DeleteTodoCommand::class);
        $command->shouldReceive('getTodoId')
            ->twice()
            ->andReturn($id);

        $todo = \Mockery::mock(Todo::class);
        $todoRepo = \Mockery::mock(TodoRepositoryInterface::class);
        $todoRepo->shouldReceive('getById')
            ->once()
            ->with($id)
            ->andReturn($todo);
        $todoRepo->shouldReceive('delete')
            ->once()
            ->with($id);

        $eventDispatcher = \Mockery::mock(EventDispatcherInterface::class);
        $eventDispatcher->shouldReceive('dispatch')
            ->once()
            ->withArgs(function ($event) use ($todo) {
                $this->assertInstanceOf(TodoDeletedEvent::class, $event);
                $this->assertEquals($todo, $event->getTodo());

                return true;
            });

        $handler = new DeleteTodoHandler($todoRepo, $eventDispatcher);
        $handler->handle($command);

        $this->assertEquals(2, Assert::getCount());
    }
}
