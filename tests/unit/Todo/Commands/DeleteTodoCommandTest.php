<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\Todo\Commands;

use App\Todo\Commands\DeleteTodoCommand;
use CodeIgniter\Test\CIUnitTestCase;

class DeleteTodoCommandTest extends CIUnitTestCase
{
    /** @test */
    public function implementsGetters()
    {
        $command = new DeleteTodoCommand('my-uuid');

        $this->assertEquals('my-uuid', $command->getTodoId());
    }
}
