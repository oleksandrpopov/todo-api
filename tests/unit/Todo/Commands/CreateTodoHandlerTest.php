<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\Todo\Commands;

use App\Todo\Commands\CreateTodoCommand;
use App\Todo\Commands\CreateTodoHandler;
use App\Todo\Entities\TodoInterface;
use App\Todo\Events\TodoCreatedEvent;
use App\Todo\Repositories\TodoRepositoryInterface;
use CodeIgniter\Test\CIUnitTestCase;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\EventDispatcherInterface;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class CreateTodoHandlerTest extends CIUnitTestCase
{
    use MockeryPHPUnitIntegration;

    /** @test */
    public function handlesCommand()
    {
        $id = 'my-uuid';
        $title = 'to something';
        $completed = true;

        $command = \Mockery::mock(CreateTodoCommand::class);
        $command->shouldReceive('getId')
            ->once()
            ->andReturn($id);
        $command->shouldReceive('getTitle')
            ->once()
            ->andReturn($title);
        $command->shouldReceive('isCompleted')
            ->once()
            ->andReturn($completed);

        $todoRepo = \Mockery::mock(TodoRepositoryInterface::class);
        $todoRepo->shouldReceive('save')
            ->once()
            ->withArgs(function ($todo) use ($id, $title, $completed) {
                $this->assertInstanceOf(TodoInterface::class, $todo);
                $this->assertEquals($id, $todo->getId());
                $this->assertEquals($title, $todo->getTitle());
                $this->assertEquals($completed, $todo->isCompleted());

                return true;
            });

        $eventDispatcher = \Mockery::mock(EventDispatcherInterface::class);
        $eventDispatcher->shouldReceive('dispatch')
            ->once()
            ->withArgs(function ($event) {
                $this->assertInstanceOf(TodoCreatedEvent::class, $event);

                return true;
            });

        $handler = new CreateTodoHandler($todoRepo, $eventDispatcher);
        $handler->handle($command);

        $this->assertEquals(5, Assert::getCount());
    }
}
