<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\Todo\Commands;

use App\Todo\Commands\UpdateTodoCommand;
use App\Todo\Commands\UpdateTodoHandler;
use App\Todo\Entities\TodoInterface;
use App\Todo\Events\TodoUpdatedEvent;
use App\Todo\Repositories\TodoRepositoryInterface;
use CodeIgniter\Test\CIUnitTestCase;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\EventDispatcherInterface;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class UpdateTodoHandlerTest extends CIUnitTestCase
{
    use MockeryPHPUnitIntegration;

    /** @test */
    public function handlesCommand()
    {
        $id = 'my-uuid';
        $title = 'to something';
        $completed = true;

        $command = \Mockery::mock(UpdateTodoCommand::class);
        $command->shouldReceive('getId')
            ->once()
            ->andReturn($id);
        $command->shouldReceive('getTitle')
            ->once()
            ->andReturn($title);
        $command->shouldReceive('isCompleted')
            ->once()
            ->andReturn($completed);

        $todo = \Mockery::mock(TodoInterface::class);
        $todo->shouldReceive('setTitle')
            ->once()
            ->with($title);
        $todo->shouldReceive('setCompleted')
            ->once()
            ->with($completed);

        $todoRepo = \Mockery::mock(TodoRepositoryInterface::class);
        $todoRepo->shouldReceive('getById')
            ->once()
            ->with($id)
            ->andReturn($todo);
        $todoRepo->shouldReceive('save')
            ->once()
            ->with($todo);

        $eventDispatcher = \Mockery::mock(EventDispatcherInterface::class);
        $eventDispatcher->shouldReceive('dispatch')
            ->once()
            ->withArgs(function ($event) {
                $this->assertInstanceOf(TodoUpdatedEvent::class, $event);

                return true;
            });

        $handler = new UpdateTodoHandler($todoRepo, $eventDispatcher);
        $handler->handle($command);

        $this->assertEquals(1, Assert::getCount());
    }
}
