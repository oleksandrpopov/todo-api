<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\Todo\Commands;

use App\Todo\Commands\UpdateTodoCommand;
use CodeIgniter\Test\CIUnitTestCase;

class UpdateTodoCommandTest extends CIUnitTestCase
{
    /** @test */
    public function implementsGetters()
    {
        $command = new UpdateTodoCommand('my-uuid', 'do something fun', true);

        $this->assertEquals('my-uuid', $command->getId());
        $this->assertEquals('do something fun', $command->getTitle());
        $this->assertTrue($command->isCompleted());
    }
}
