<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\Todo\Entities;

use App\Todo\Entities\Todo;
use CodeIgniter\Test\CIUnitTestCase;

class TodoTest extends CIUnitTestCase
{
    /** @test */
    public function implementsGettersAndSetters()
    {
        $todo = new Todo();
        $todo->setId('my-uuid');
        $todo->setTitle('do something fun');
        $todo->setCompleted(true);

        $this->assertEquals('my-uuid', $todo->getId());
        $this->assertEquals('do something fun', $todo->getTitle());
        $this->assertTrue($todo->isCompleted());
    }
}
