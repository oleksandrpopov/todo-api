<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\Todo\Events;

use App\Todo\Entities\TodoInterface;
use App\Todo\Events\TodoEvent;
use CodeIgniter\Test\CIUnitTestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class TodoEventTest extends CIUnitTestCase
{
    use MockeryPHPUnitIntegration;

    /** @test */
    public function implementsGetter()
    {
        $todo = \Mockery::mock(TodoInterface::class);
        $event = new TodoEvent($todo);

        $this->assertEquals($todo, $event->getTodo());
    }
}
