<?php
/**
 * This source file is part of todo.
 * Copyright (c) 2020.
 * All rights reserved.
 */

namespace Tests\Unit\Todo\Repositories;

use App\Todo\Entities\TodoInterface;
use App\Todo\Exceptions\TodoNotFoundException;
use App\Todo\Repositories\TodoRepository;
use CodeIgniter\Database\BaseBuilder;
use CodeIgniter\Database\ResultInterface;
use CodeIgniter\Test\CIUnitTestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class TodoRepositoryTest extends CIUnitTestCase
{
    use MockeryPHPUnitIntegration;

    /**
     * @var BaseBuilder|\Mockery\MockInterface
     */
    private $queryBuilder;

    /**
     * @var TodoRepository|\Mockery\MockInterface
     */
    private $todoRepo;

    /** @test */
    public function deletesTodo()
    {
        $id = 'my-uuid';
        $this->queryBuilder->shouldReceive('delete')
            ->once()
            ->with(['id' => $id]);

        $this->todoRepo->delete($id);
    }

    /** @test */
    public function returnsAllTodos()
    {
        $rows = [
            (object)[
                'id' => 'my-id',
                'title' => 'do something',
                'completed' => '1',
            ],
        ];
        $result = \Mockery::mock(ResultInterface::class);
        $result->shouldReceive('getResult')
            ->once()
            ->andReturn($rows);
        $this->queryBuilder->shouldReceive('get')
            ->once()
            ->andReturn($result);

        $result = $this->todoRepo->getAll();

        $this->assertCount(1, $result);
        $this->assertInstanceOf(TodoInterface::class, $result[0]);
        $this->assertEquals('my-id', $result[0]->getId());
        $this->assertEquals('do something', $result[0]->getTitle());
        $this->assertTrue($result[0]->isCompleted());
    }

    /** @test */
    public function findsTodoById()
    {
        $id = 'my-id';
        $row = (object)[
            'id' => $id,
            'title' => 'do something',
            'completed' => '1',
        ];
        $result = \Mockery::mock(ResultInterface::class);
        $result->shouldReceive('getRow')
            ->once()
            ->andReturn($row);
        $this->queryBuilder->shouldReceive('getWhere')
            ->once()
            ->with(['id' => $id])
            ->andReturn($result);

        $todo = $this->todoRepo->findById($id);

        $this->assertInstanceOf(TodoInterface::class, $todo);
        $this->assertEquals('my-id', $todo->getId());
        $this->assertEquals('do something', $todo->getTitle());
        $this->assertTrue($todo->isCompleted());
    }

    /** @test */
    public function returnsNullWhenUnableToFindTodoById()
    {
        $id = 'my-id';
        $result = \Mockery::mock(ResultInterface::class);
        $result->shouldReceive('getRow')
            ->once()
            ->andReturnNull();
        $this->queryBuilder->shouldReceive('getWhere')
            ->once()
            ->with(['id' => $id])
            ->andReturn($result);

        $this->assertNull($this->todoRepo->findById($id));
    }

    /** @test */
    public function savesNewTodo()
    {
        $id = 'my-uuid';
        $title = 'to something';
        $completed = true;

        $todo = \Mockery::mock(TodoInterface::class);
        $todo->shouldReceive('getId')
            ->twice()
            ->andReturn($id);
        $todo->shouldReceive('getTitle')
            ->once()
            ->andReturn($title);
        $todo->shouldReceive('isCompleted')
            ->once()
            ->andReturn($completed);

        $this->queryBuilder->shouldReceive('insert')
            ->once()
            ->with([
                'id' => $id,
                'title' => $title,
                'completed' => $completed,
            ]);

        $this->todoRepo->shouldReceive('todoExists')
            ->once()
            ->with($id)
            ->andReturnFalse();

        $this->todoRepo->save($todo);
    }

    /** @test */
    public function savesExistingTodo()
    {
        $id = 'my-uuid';
        $title = 'to something';
        $completed = true;

        $todo = \Mockery::mock(TodoInterface::class);
        $todo->shouldReceive('getId')
            ->twice()
            ->andReturn($id);
        $todo->shouldReceive('getTitle')
            ->once()
            ->andReturn($title);
        $todo->shouldReceive('isCompleted')
            ->once()
            ->andReturn($completed);

        $this->queryBuilder->shouldReceive('update')
            ->once()
            ->with(['title' => $title, 'completed' => $completed], ['id' => $id]);

        $this->todoRepo->shouldReceive('todoExists')
            ->once()
            ->with($id)
            ->andReturnTrue();

        $this->todoRepo->save($todo);
    }

    /** @test */
    public function throwsExceptionWhenUnableToReturnTodoById()
    {
        $id = 'my-uuid';
        $this->expectException(TodoNotFoundException::class);
        $this->todoRepo->shouldReceive('findById')
            ->once()
            ->with($id)
            ->andReturnNull();
        $this->todoRepo->getById($id);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->queryBuilder = \Mockery::mock(BaseBuilder::class);
        $this->todoRepo = \Mockery::mock(TodoRepository::class, [$this->queryBuilder])
            ->shouldAllowMockingProtectedMethods()
            ->makePartial();
    }
}
